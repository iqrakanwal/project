from django.urls import path

from . import views




urlpatterns = [
    path('result/', views.search , name="search"),
    path('Post/', views.projectpost , name="projectpost"),
    path('allprojects/', views.allprojects , name="allprojects"),
    path('showproject/<int:id>/', views.showproject , name="showproject"),
    path('bidform/<int:id>/', views.bidform , name="bidform"),
    path('buyerprojects/', views.buyerprojects , name="buyerprojects"),
    path('deletpro/<int:id>/', views.deletpro , name="deletpro"),
    path('editpro/<int:id>/', views.editpro , name="editpro"),
    
    
    path('bids/delete/<int:id>/', views.biddelete , name="biddelete"),
    path('bidsfreelancer/', views.bidsfreelancer , name="bidsfreelancer"),
    path('freebiddescrition/<int:id>/', views.freebiddescrition , name="freebiddescrition"),
    path('invitefreelancer/<int:id>/', views.invitefreelancer , name="invitefreelancer"),
    path('invitefreelancer/confirmation/<int:projid>/<int:userid>/', views.confirmation , name="confirmation"),
    path('invitefreelancer/searchinvite/', views.searchinvite , name="searchinvite"),
    
    path('setprogress/<int:id>/', views.setprogress , name="setprogress"),
    path('deletbid/<int:id>/', views.deletbid , name="deletbid"),
    path('bids/<int:id>/', views.bids , name="bids"),
    path('progress/<int:id>/', views.progress , name="progress"),
    path('category/<slug:name>/', views.category , name="category"),
    path('message/<int:id>/', views.message , name="message"),
    
]  