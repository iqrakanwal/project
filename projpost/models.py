from django.db import models
from datetime import datetime


# Create your models here.


class invitations(models.Model):    
     projid=models.IntegerField()
     freelancid=models.IntegerField()
     title = models.CharField(max_length=100, default='')
     

class Posted_proj(models.Model):
    id=models.AutoField(primary_key=True)
    user_id = models.IntegerField(default=0)
    budget = models.IntegerField(default=0)
    title = models.CharField(max_length=100, default='')
    description = models.CharField(max_length=100, default='')
    file = models.FileField(upload_to='project_pdf)', blank=True)
    submit_time = models.CharField(max_length=100, default='')
    Django = models.CharField(max_length=100, default='')
    asp = models.CharField(max_length=100, default='')
    Php = models.CharField(max_length=100, default='')
    MEANStack = models.CharField(max_length=100, default='')
    Javascript = models.CharField(max_length=100, default='')
    HTML = models.CharField(max_length=100, default='')
    CSS = models.CharField(max_length=100, default='')

class bidform(models.Model):  
     owner_id = models.IntegerField()
     unique_id = models.IntegerField(primary_key=True)
     user_id = models.IntegerField()
     price = models.IntegerField(default=0)
     coverletter = models.CharField(max_length=100, default='')
     cv = models.FileField(upload_to='cv)', blank=True)
     projid=models.IntegerField(default=0)
     class Meta:
        unique_together = (("user_id", "projid"),)


class contract(models.Model):
     title = models.CharField(max_length=100, default='')    
     proj_id = models.IntegerField(primary_key=True)
     user_id = models.IntegerField()
     start_time= models.CharField(max_length=100, default='')
     status=models.CharField(max_length=100, default='')
     progress = models.IntegerField(default=0)
     final_work = models.FileField(upload_to='cv)', blank=True)
     owner_id=models.IntegerField()


class finalCode(models.Model):    
     finalCode = models.FileField(upload_to='final_work', blank=True)
     projid=models.IntegerField(primary_key=True)
     status=models.CharField(max_length=100, default=0)


class notifications(models.Model):    
     projid=models.IntegerField(primary_key=True)
     status=models.CharField(max_length=100, default=0)
     deadline = models.CharField(max_length=100, default='')
     Django = models.CharField(max_length=100, default='')
     asp = models.CharField(max_length=100, default='')
     Php = models.CharField(max_length=100, default='')
     MEANStack = models.CharField(max_length=100, default='')
     Javascript = models.CharField(max_length=100, default='')
     HTML = models.CharField(max_length=100, default='')
     CSS = models.CharField(max_length=100, default='')


class savemessages(models.Model):    
     projid = models.FileField(upload_to='final_work', blank=True)
     user=models.CharField(max_length=100, default=0)
     message=models.CharField(max_length=100, default=0)     
     notify = models.IntegerField(default=0)