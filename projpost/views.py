from django.shortcuts import render
from projpost.models import Posted_proj ,bidform ,contract ,finalCode  ,savemessages ,invitations
import json
from authenticate.models import UserProfile
from django.http import HttpResponse
from authenticate.models import UserProfile ,Qualities 
import datetime
from django.utils.timezone import utc
from django.db.models import Q
from django.views.decorators.csrf import csrf_exempt
from django.conf.urls import url
import json as simplejson
# Create your views here.
def category(request, name):
    category_name=''
    try:
        if name=='django':
           category_name='All Django Projects ' 
           test_results = Posted_proj.objects.filter(Django='Django')
        if name=='asp':  
           category_name='All Asp.Net Projects '                                                                                                   
           test_results = Posted_proj.objects.filter(asp='asp')
        if name=='meanstack':
           category_name='All MeanStack Projects ' 
           test_results = Posted_proj.objects.filter(MEANStack='MEANStack')
        if name=='php':
           category_name='All PHP Projects ' 
           test_results = Posted_proj.objects.filter(Php='Php')
        if name=='css':
           category_name='All CSS Projects ' 
           test_results = Posted_proj.objects.filter(CSS='CSS')
        if name=='javascript':
           category_name='All JavaScript Projects ' 
           test_results = Posted_proj.objects.filter(Javascript='Javascript')
        if name=='html':
           category_name='All Html Projects ' 
           test_results = Posted_proj.objects.filter(HTML='HTML')

        snippet = []
        for k in test_results:
            snippet.append(k)
    except:
           print(snippet) 

    if not snippet:
           snippet='None'
           category_name=''
 

    return render(request, 'all_projects.html', {'snippet': snippet , 'category_name' : category_name})



def projectpost(request):
    alert=0
    profile = UserProfile.objects.get(user_id=request.user.id)
    my=[];
    if request.method == 'POST':
       proj=Posted_proj()	
       proj.user_id=request.user.id
       proj.budget=request.POST['budget']
       proj.title=request.POST['title']
       proj.description=request.POST['description']
       proj.file=request.FILES['File']
       proj.submit_time=request.POST['submissiondata']

       proj.Django=request.POST['Django']
       proj.asp=request.POST['Asp']
       proj.Php=request.POST['php']
       proj.MEANStack=request.POST['meanStack']
       proj.Javascript=request.POST['Javascript']
       proj.HTML=request.POST['html']
       proj.CSS=request.POST['css']
       alert=1
       proj.save()
       
       
    return render(request, 'projpost.html' , {'profile':profile , 'alert':alert})



def allprojects(request):

    snippet = Posted_proj.objects.all()
    
    if not snippet:
       snippet="None"
    return render(request, 'all_projects.html' ,{'snippet':snippet})


def showproject(request, id):
    project = Posted_proj.objects.get(id=id)
    ownerid=project.user_id
    profile=UserProfile.objects.get(user_id=request.user.id)  
    already=0
    try:
          already = bidform.objects.get(projid=id , user_id=request.user.id )
    except:
          print('error')
   
    cont=0      
    try:
          cont = contract.objects.get(proj_id=id) 
    except:
          print('error')
    alert=0  
    updat=bidform.objects.filter( user_id =  request.user.id  ,  projid = id)
     
    if not updat:
      
       if request.method == 'POST':
          bid = bidform()
          bid.user_id=request.user.id
          bid.price=request.POST['price']
          bid.coverletter=request.POST['description']
          bid.cv=request.FILES['File']
          bid.projid=id
          bid.owner_id=ownerid
          bid.save() 
          bidcount=profile.remainbids
          bidc=bidcount-1
          UserProfile.objects.filter(user_id =  request.user.id).update(remainbids=bidc)
          alert=1

    else:
        if request.method == 'POST':
            updat = {'price': request.POST['price'], 'coverletter': request.POST['description'],  'cv' :  request.FILES['File']}
            bidform.objects.filter( user_id =  request.user.id  ,  projid = id).update(**updat)
            alert=1

          
    return render(request, 'projectsdescription.html' ,{'project':project  , 'cont' : cont , 'already':already   , 'alert' :alert , 'profile':profile})    
   
def buyerprojects(request):
    cont=0
    snippet = Posted_proj.objects.all()
    profile = UserProfile.objects.get(user_id=request.user.id)
    try:
       matched=[]
       for k in snippet:
           if  k.user_id == request.user.id :
               matched.append(k)
    except:
          print('error')
    
    
    
    return render(request, 'buyerprojects.html', {'matched':matched , 'profile': profile }) 


def deletpro(request,id):
    alert=0
    if request.method == 'POST':
        pro=Posted_proj.objects.get(id=id)
        pro.delete()
        alert=1

    try:
          cont = contract.objects.get(proj_id=id) 
    except:
          cont = 0       
    return render(request, 'deletepro.html', { 'cont'  : cont  , 'alert' : alert}) 


def editpro(request,id):
    alert=0
    cont=0
    project = Posted_proj.objects.get(id=id)
    if request.method == 'POST':
       proj=Posted_proj() 
       proj.id=id 
       proj.user_id=request.user.id
       proj.budget=request.POST['budget']
       proj.title=request.POST['title']
       proj.description=request.POST['description']
       proj.submit_time=request.POST['submissiondata']

       proj.Django=request.POST['Django']
       proj.asp=request.POST['Asp']
       proj.Php=request.POST['php']
       proj.MEANStack=request.POST['meanStack']
       proj.Javascript=request.POST['Javascript']
       proj.HTML=request.POST['html']
       proj.CSS=request.POST['css']
       alert=1
       proj.save()

    try:
          cont = contract.objects.get(proj_id=id) 
    except:
          cont = 0       
    return render(request, 'editpro.html', { 'cont'  : cont  , 'alert' : alert , 'project' : project}) 






def bids(request , id):
    project = Posted_proj.objects.get(id=id)
    if request.method == 'POST':
       contr=contract()
       contr.title=project.title
       contr.user_id=request.POST['uservalue']
       contr.proj_id=id
       contr.start_time=datetime.datetime.utcnow().replace(tzinfo=utc)
       contr.status='Progress'
       contr.owner_id=project.user_id
       contr.save() 
 

    bid = bidform.objects.all()
    profile = UserProfile.objects.get(user_id=request.user.id)
    try:
       matched=[]
       for k in bid:
           if  k.projid == id:
               matched.append(k)
    except:
          print('error')
    contr=0      
    try:
          contr = contract.objects.get(proj_id=id) 
    except:
          print('error')

    if not matched:
       matched="None"

    return render(request, 'reqbids.html', {'matched':matched , 'profile': profile , 'contr':contr})     


def biddelete(request ,id ):
    alert=0
    instance = bidform.objects.get(unique_id=id)
    instance.delete() 
    
    cont=0
    snippet = Posted_proj.objects.all()
    profile = UserProfile.objects.get(user_id=request.user.id)
    try:
       matched=[]
       for k in snippet:
           if  k.user_id == request.user.id :
               matched.append(k)
    except:
          print('error')
    alert=1
    return render(request , 'buyerprojects.html' , {'alert':alert  , 'matched':matched , 'profile': profile })
    

def progress(request , id):
    if request.method == 'POST':
       if 'progress' in request.POST:
           contract.objects.filter(proj_id=id).update(progress=request.POST['progress']) 
       elif 'final_work' in request.FILES:
           fc=finalCode()
           fc.finalCode=request.FILES['final_work']  
           fc.projid=id
           fc.status='Completed'
           fc.save()
    
    bid=0
    time=0
    contr=0   
    try:
         profile = UserProfile.objects.get(user_id=request.user.id) 
         contr = contract.objects.get(proj_id=id) 
         bid = bidform.objects.get(projid=id)
         time = Posted_proj.objects.get(id=id)
         time=time.submit_time
         fc=0
    except:
        print("Error")
    
    fc=0
    try:
        fc= finalCode.objects.get(projid=id)
    except:
        print("Hello, World!")
    
    return render(request, 'progress.html', {'bid':bid , 'time' : time  , 'profile' : profile , 'contr' :contr , 'fc':fc })


def search(request):
    template='all_projects.html'
    query=request.GET.get('q')
    if query:
       snippet= Posted_proj.objects.filter(Q(title__icontains=query) |  Q(description__icontains=query ) |  Q(Javascript__icontains=query)  |  Q(MEANStack__icontains=query ) |  Q(Django__icontains=query )  |  Q(asp__icontains=query ) |  Q(HTML__icontains=query ) |  Q(CSS__icontains=query ) |  Q(Php__icontains=query ) )
    else :
       print('there is nothing') 

    if not snippet:
       snippet="None"

    return render(request , template , {'snippet':snippet})




@csrf_exempt
def message(request,id):
    import json
    url = request.get_full_path()
    myid=url.split('/')[-2]
    chatdata=contract.objects.filter(proj_id=myid)
    # print (chatdata.get().user_id)
    # print (chatdata.get().owner_id)
    
    
    if request.method == 'POST':
       if chatdata.get().user_id == request.user.id :
          notifyid=chatdata.get().owner_id
       else:
          notifyid=chatdata.get().user_id
             
       saveme=savemessages()
       saveme.projid=myid
       saveme.user=request.user.first_name
       saveme.message=request.POST.get('message')
       saveme.notify=notifyid
       print(chatdata.get().user_id)
       print(chatdata.get().owner_id)
       print(request.user.id)
       print(notifyid)
       saveme.save()
       
    # JSONdata = request.POST['data']
    # dict = simplejson.JSONDecoder().decode( JSONdata ) 
    # username = dict['message']
    # print(JSONdata)

    # print(request.POST.get("message"))  
   
 
    return HttpResponse(content_type="application/json" )




def bidsfreelancer(request):
    freebids=bidform.objects.filter(user_id= request.user.id)

    if not freebids:
       freebids="None"

    return render(request, 'freelancerbids.html', {'freebids':freebids })    


def freebiddescrition(request ,id ):
    alert=0
    freebidsdes = bidform.objects.get(projid=id )
    project = Posted_proj.objects.get(id=id)
    
    cont=0      
    try:
          cont = contract.objects.get(proj_id=id) 
    except:
          print('error')

    try:
        if request.method == 'POST':
            updat = {'price': request.POST['price'], 'coverletter': request.POST['description'],
                 'cv': request.FILES['File']}
            bidform.objects.filter(user_id=request.user.id, projid=id).update(**updat)
            alert = 1
    except:
          print('error')


    return render(request, 'freelancerbiddescription.html', {'freebidsdes':freebidsdes , 'project':project ,  'cont': cont , 'alert' :alert })



def deletbid(request ,id):
    instance = bidform.objects.get(projid=id)
    instance.delete()   
    freebids=bidform.objects.filter(user_id= request.user.id)
    return render(request, 'freelancerbids.html', {'freebids':freebids })



@csrf_exempt
def setprogress(request ,id):
    try:
         finalCode.objects.filter(projid=id).update(status="EXPIRED")
    except:
          print('error')
    
    contract.objects.filter(proj_id=id).update(status="EXPIRED") 

    if request.method == 'POST':
       if 'progress' in request.POST:
           contract.objects.filter(proj_id=id).update(progress=request.POST['progress']) 
       elif 'final_work' in request.FILES:
           fc=finalCode()
           fc.finalCode=request.FILES['final_work']  
           fc.projid=id
           fc.status='Completed'
           fc.save()
    
    bid=0
    time=0
    contr=0   
    try:
         profile = UserProfile.objects.get(user_id=request.user.id) 
         contr = contract.objects.get(proj_id=id) 
         bid = bidform.objects.get(projid=id)
         time = Posted_proj.objects.get(id=id)
         time=time.submit_time
         fc=0
    except:
        print("Error")
    
    fc=0
    try:
        fc= finalCode.objects.get(projid=id)
    except:
        print("Hello, World!")
    
    return render(request, 'progress.html', {'bid':bid , 'time' : time  , 'profile' : profile , 'contr' :contr , 'fc':fc })


def invitefreelancer(request,id): 
    invite=UserProfile.objects.filter(role_user=1);
    projid=id
    return render(request, 'invitefreelancers.html', {'invite':invite  , 'projid' : projid })

def confirmation(request,projid, userid):
    alert=0
    if request.method == 'POST':
       inv=invitations()
       inv.projid=request.POST['projid']
       inv.freelancid=request.POST['freelancid']
       inv.title=request.POST['title']
       inv.save()
       alert=1
 
    return render(request, 'confirmationinvite.html', {'userid':userid  , 'projid' : projid , 'alert' :alert })


def searchinvite(request):
    template='invitefreelancers.html'
    query=request.GET.get('q')
    if query:
       snippet= Qualities.objects.filter(  Q(Javascript__icontains=query)  |  Q(MEANStack__icontains=query ) |  Q(Django__icontains=query )  |  Q(asp__icontains=query ) |  Q(HTML__icontains=query ) |  Q(CSS__icontains=query ) |  Q(Php__icontains=query ) )
    else :
       print('there is nothing') 

    if not snippet:
       snippet="None"

    return render(request , template , {'snippet':snippet})