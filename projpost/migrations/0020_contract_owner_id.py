# Generated by Django 2.0.7 on 2018-10-23 14:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('projpost', '0019_auto_20181018_1829'),
    ]

    operations = [
        migrations.AddField(
            model_name='contract',
            name='owner_id',
            field=models.IntegerField(default=1),
            preserve_default=False,
        ),
    ]
