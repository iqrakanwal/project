# Generated by Django 2.0.7 on 2018-09-30 13:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('projpost', '0008_contract_status'),
    ]

    operations = [
        migrations.AlterField(
            model_name='posted_proj',
            name='submit_time',
            field=models.CharField(default='', max_length=100),
        ),
    ]
