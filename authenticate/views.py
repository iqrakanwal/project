from django.shortcuts import render,redirect
from django.contrib.auth import authenticate,login,logout
from django.contrib import  messages
from django.contrib.auth.forms import UserCreationForm  , UserChangeForm , PasswordChangeForm 
from .forms import SignUpForm , EditProfileForm ,ProfileForm
from . import models
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AdminPasswordChangeForm, PasswordChangeForm
from django.contrib.auth import update_session_auth_hash
from django.contrib import messages
from django.shortcuts import render, redirect

from social_django.models import UserSocialAuth
import json
from authenticate.models import UserProfile ,Qualities
from django.views.decorators.csrf import csrf_exempt
from snippets.models import snippets ,changerequest
from projpost.models import Posted_proj , contract ,  bidform ,savemessages ,invitations
from django.http import JsonResponse
from django.core import serializers
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.core.paginator import Paginator
from django.http import HttpResponseRedirect
# Create your views here.

def home(request):
  snippet = snippets.objects.all()

  try:
      matched=[]
      for k in snippet:
           if  k.sniptype == 'free' :
               matched.append(k)
  except:
       print('error')

  paginator = Paginator(matched, 6)
  page = request.GET.get('page')
  matched = paginator.get_page(page)

  return render(request, 'authenticate/home.html',{'snippet':matched})

@login_required
def settings(request):
    user = request.user

    try:
        github_login = user.social_auth.get(provider='github')
    except UserSocialAuth.DoesNotExist:
        github_login = None

    try:
        twitter_login = user.social_auth.get(provider='twitter')
    except UserSocialAuth.DoesNotExist:
        twitter_login = None

    try:
        facebook_login = user.social_auth.get(provider='facebook')
    except UserSocialAuth.DoesNotExist:
        facebook_login = None

    can_disconnect = (user.social_auth.count() > 1 or user.has_usable_password())

    return render(request, 'authenticate/settings.html', {
        'github_login': github_login,
        'twitter_login': twitter_login,
        'facebook_login': facebook_login,
        'can_disconnect': can_disconnect
    })


def password(request):
    if request.user.has_usable_password():
        PasswordForm = PasswordChangeForm
    else:
        PasswordForm = AdminPasswordChangeForm

    if request.method == 'POST':
        form = PasswordForm(request.user, request.POST)
        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)
            messages.success(request, 'Your password was successfully updated!')
            return redirect('Addprofile')
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = PasswordForm(request.user)
       # return redirect('add_profile')
    return render(request, 'authenticate/password.html', {'form': form})

def login_user(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
           login(request, user)
           messages.success(request, ('Welcome Back Freelancer'))
           return redirect('home')
           # Redirect to a success page.
        
        else:
           messages.success(request, ('Error logging in - Please try again'))
           return redirect('login')
       

    else:
       return render(request, 'authenticate/login.html',{})	



def logout_user(request):
	logout(request)
	messages.success(request , ('Freelancer Logout successfully'))
	return redirect('home')



def  register_user(request):
     if request.method == 'POST':
        form = SignUpForm(request.POST)  
        if  form.is_valid():
             form.save()
             username=form.cleaned_data['username']
             password=form.cleaned_data['password1']   
             user = authenticate(request, username=username, password=password)
             login(request, user)             
             messages.success(request, ('Welcome Freelancer'))


             return redirect('add_profile')
              
     else:
         form=SignUpForm()

     context = { 'form' : form }       

     return render(request, 'authenticate/register.html', context)	



def edit_profile(request):

    if request.method == 'POST':
        form = EditProfileForm(request.POST, instance=request.user)
        if form.is_valid():
           form.save()
           messages.success(request, ('Changes Made Successfully'))
           return redirect('home')

    else:
      form=EditProfileForm(instance=request.user)

    context = {'form' : form, }

    return render(request, 'authenticate/edit_profile.html', context)



def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(data=request.POST, user=request.user)
        if form.is_valid():
           form.save()
           messages.success(request, ('Password  Changed Successfully '))
           return redirect('home')
    else:
      form=PasswordChangeForm(user=request.user)
    context = { 'form' : form}

    return render(request, 'authenticate/change_password.html', context)




def Addprofile(request):
    usertype = []
    usertype.append("Freelancer")
    usertype.append("Buyer")
    
    if request.method == 'POST':
        user = UserProfile()
        user.user_id=request.user.id
        user.description= request.POST['description']
        user.city = request.POST['city']
        user.phone=request.POST['phone']
        user.image = request.FILES['image']
        user.image_name = request.FILES['image']._name

        if request.POST['role_user'] == 'Freelancer':
            user.role_user=1
        else:
            user.role_user=0

        user.website=request.POST['website']

        user.save()

        qual=Qualities()
        qual.user_id=request.user.id 
        qual.Django=request.POST['Django']
        qual.asp=request.POST['Asp']
        qual.Php=request.POST['php']
        qual.MEANStack=request.POST['meanStack']
        qual.Javascript=request.POST['Javascript']
        qual.HTML=request.POST['html']
        qual.CSS=request.POST['css']
        qual.save()


        return redirect('home')

    return render(request, 'authenticate/additional_features.html', { 'usertype' : usertype})




def Userprofile(request):
    profile = UserProfile.objects.get(user_id=request.user.id)
    qualities = Qualities.objects.get(user_id=request.user.id)
    snippet = snippets.objects.all()
    try:
       matched=[]
       for k in snippet:
           if  k.user_id == request.user.id :
               matched.append(k)
    except:
          print('error')

    
    try:
        projs=Posted_proj.objects.filter(user_id=request.user.id) 

    except:
        projs="None"  

    return render(request, 'authenticate/user_profile.html', { 'profile': profile , 'matched':matched , 'qualities':qualities  } )


def Userprofile(request, id):
    usert = User.objects.get(id=id)
    profile = UserProfile.objects.get(user_id=id)
    qualities = Qualities.objects.get(user_id=id)

    snippet = snippets.objects.all()
    try:
        matched = []
        for k in snippet:
            if k.user_id == id:
                matched.append(k)
    except:
        print('error')

    try:
        projs=Posted_proj.objects.filter(user_id=id)

    except:
        projs="None"


    try:
        projcon= contract.objects.filter(user_id=id)
    except:
        projcon="None"    

    return render(request, 'authenticate/user_profile.html',
                  {'profile': profile, 'matched': matched, 'qualities': qualities, 'usert': usert   , 'projs' :projs  , 'projcon':projcon })


def editprofile(request, id):
  

    profile = UserProfile.objects.get(user_id=id)
    qualities = Qualities.objects.get(user_id=id)
    alert=0
    if request.method == 'POST':
        user = UserProfile()
        user.user_id=request.user.id
        user.description= request.POST['description']
        user.city = request.POST['city']
        user.phone=request.POST['phone']
        user.image = profile.image
        user.role_user=profile.role_user
        user.website=request.POST['website']
        user.save()
        qual=Qualities()
        qual.user_id=request.user.id 
        qual.Django=request.POST['Django']
        qual.asp=request.POST['Asp']
        qual.Php=request.POST['php']
        qual.MEANStack=request.POST['meanStack']
        qual.Javascript=request.POST['Javascript']
        qual.HTML=request.POST['html']
        qual.CSS=request.POST['css']
        qual.save()
        alert=1

    return render(request, 'authenticate/savefeatures.html',
                  {'profile': profile,'qualities': qualities , 'alert' :alert })



@csrf_exempt
def edit_pro(request):
    import json
    
    profile = UserProfile.objects.get(user_id=request.user.id)

    invite=""
    usertype=""
    if profile.role_user == 1:
       try:
            invite= invitations.objects.filter(freelancid=request.user.id)
            invite=serializers.serialize('json',invite)
            mydata=Posted_proj.objects.all()
            mydata=serializers.serialize('json',mydata)
            usertype='Freelancer'
       except:
            invite=""

    else :
       mydata=bidform.objects.filter(owner_id=request.user.id)
       mydata=serializers.serialize('json',mydata)
       usertype='Buyer'

    context = {
          'mydata':mydata ,
          'usertype':usertype,
          'invite':invite
               }
    return HttpResponse(json.dumps(context), content_type="application/json")     


@csrf_exempt
def snippetrequests(request):
    import json
    print(request)
    data={}
    mydata=changerequest.objects.filter(owner_id=request.user.id)
    yourdata=changerequest.objects.filter(user_id=request.user.id)
    mydata=serializers.serialize('json',mydata)
    yourdata=serializers.serialize('json',yourdata)

    context = {
        'mydata':mydata ,
        'yourdata':yourdata,
           }
    return HttpResponse(json.dumps(context), content_type="application/json" ) 


@csrf_exempt
def activity(request):
    import json
    data={}
    profile = UserProfile.objects.get(user_id=request.user.id)
    usertype=""
    if profile.role_user == 1:

       mydata=contract.objects.filter(user_id=request.user.id)
       mydata=serializers.serialize('json',mydata)
       usertype='Freelancer'

    else :
       mydata=contract.objects.filter(owner_id=request.user.id)
       mydata=serializers.serialize('json',mydata)
       yourdata=contract.objects.filter(owner_id=request.user.id)
       usertype='Buyer'

    context = {
        'mydata':mydata ,
        'usertype':usertype,
           }
    return HttpResponse(json.dumps(context), content_type="application/json" )     



@csrf_exempt
def newmessage(request):
    import json
    
    savem = savemessages.objects.filter(notify=request.user.id)    
    mydata=serializers.serialize('json',savem)
    context = {
          'mydata':mydata ,    
               }
    return HttpResponse(json.dumps(context), content_type="application/json")        


