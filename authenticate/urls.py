from django.urls import path,include

from . import views
from django.conf import settings
from django.conf.urls.static import static



urlpatterns = [
    path('', views.home , name="home"),
    path('login/', views.login_user , name="login"),
    path('logout/', views.logout_user , name="logout"),
    path('oauth/', include('social_django.urls', namespace='social')),
    path('register/', views.register_user , name="register"),
    path('edit_profile/', views.edit_profile , name="edit_profile"),
    path('change_password/', views.change_password , name="change_password"),
    path('add_profile/', views.Addprofile , name="add_profile"),
    #path('edit/<int:pk>', views.ProUpdate.as_view(), name='Update_profile'),
    path('Userprofile/', views.Userprofile , name="Userprofile"),
    path('Userprofile/<int:id>/', views.Userprofile , name="Userprofile"),
    path('editprofile/<int:id>/', views.editprofile , name="editprofile"),
    
    path('edit_pro/', views.edit_pro , name="edit_pro"),
    path('snippetrequests/', views.snippetrequests , name="snippetrequests"),
    path('activity/', views.activity , name="activity"),
    path('newmessage/', views.newmessage , name="newmessage"),
    path('settings/', views.settings, name='settings'),
    path('settings/password/', views.password, name='password'),

]  + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
