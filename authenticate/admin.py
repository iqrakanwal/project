from django.contrib import admin

from . models import  UserProfile,Qualities

admin.site.site_header = "CODESNIPPETS Admin"
admin.site.site_title = "CODESNIPPETS Admin Portal"

admin.site.index_title = "Welcome to CODESNIPPETS Portal"
admin.site.color = "#6495ED"
admin.site.register(UserProfile)

admin.site.register(Qualities)

