from django.db import models
from django.contrib.auth.models import User

class UserProfile(models.Model):
	 remainbids=models.IntegerField(default=5)
	 user_id = models.IntegerField(primary_key=True)
	 description = models.CharField(max_length=100 , default='')
	 city = models.CharField(max_length=100 , default='')
	 website=models.URLField(default='')
	 phone=models.IntegerField(default=0)
	 role_user=models.IntegerField(default=0)
	 image=models.ImageField(upload_to='profile_image' , blank=True)
	 image_name=models.CharField(max_length=100 , default='')
     



class Qualities(models.Model):
	 user_id = models.IntegerField(primary_key=True)
	 Django = models.CharField(max_length=100 , default='')
	 asp = models.CharField(max_length=100 , default='')
	 Php = models.CharField(max_length=100 , default='')
	 MEANStack = models.CharField(max_length=100 , default='')
	 Javascript = models.CharField(max_length=100 , default='')
	 HTML = models.CharField(max_length=100 , default='')
	 CSS = models.CharField(max_length=100 , default='')
	
