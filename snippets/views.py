from django.shortcuts import render
from snippets.models import snippets , snippetsViewer , changerequest , feedback ,comment
from authenticate.models import UserProfile 
# Create your views here.
from django.shortcuts import redirect
from django.db.models import Q
from django.core.paginator import Paginator
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponseRedirect

def snipcategory(request, name):
    category_name=''
    try:
        if name=='django':
           category_name='All Django snippets ' 
           test_results = snippets.objects.filter(Django='Django')
        if name=='asp':  
           category_name='All Asp.Net snippets '                                                                                                   
           test_results = snippets.objects.filter(asp='asp')
        if name=='meanstack':
           category_name='All MeanStack snippets ' 
           test_results = snippets.objects.filter(MEANStack='meanStack')
        if name=='php':
           category_name='All PHP snippets ' 
           test_results = snippets.objects.filter(Php='php')
        if name=='css':
           category_name='All CSS snippets ' 
           test_results = snippets.objects.filter(CSS='css')
        if name=='javascript':
           category_name='All JavaScript snippets ' 
           test_results = snippets.objects.filter(Javascript='Javascript')
        if name=='html':
           category_name='All Html snippets ' 
           test_results = snippets.objects.filter(HTML='html')
                 

        snippet = []
        for k in test_results:
            snippet.append(k)
    except:
        print('error')

      

  
    if not snippet:
       snippet="None"

    return render(request, 'all_snippets.html', {'snippet': snippet , 'category_name' : category_name})









def snippet(request):

    snippet = snippets.objects.all()
    paginator = Paginator(snippet, 4)
    page = request.GET.get('page')
    snippet = paginator.get_page(page)

    if not snippet:
       snippet="None"


    return render(request, 'all_snippets.html' ,{'snippet':snippet})



def createSnippet(request):
    alert=0
    if request.method == 'POST':
        snippet = snippets()
        snippet.codeoptimization = request.POST['codeop']
        snippet.user_id= request.user.id
        snippet.description= request.POST['description']
        snippet.name = request.POST['name']
        snippet.code= request.POST['code']

        snippet.title = request.POST['title']
        snippet.image= request.FILES['image']
        snippet.image_name=request.FILES['image']._name
        snippet.sniptype = request.POST['code']
        snippet.usermanual = request.FILES['File']
        snippet.Django=request.POST['Django']
        snippet.asp=request.POST['Asp']
        snippet.Php=request.POST['php']
        snippet.MEANStack=request.POST['meanStack']
        snippet.Javascript=request.POST['Javascript']
        snippet.HTML=request.POST['html']
        snippet.CSS=request.POST['css']
  

        
        if request.POST['role_snip'] == 'free':
            snippet.sniptype='free'
        else:
            snippet.sniptype='paid' 


        snippet.save() 
        descri=snippetsViewer()
        descri.user_id=request.user.id    
        descri.buyer=1
        descri.snipId=snippet.id
        descri.save()              
        alert=1

    return render(request, 'create_snippets.html'  , { 'alert' : alert } )


def snip_descri(request ,id):
    try:
         profile = UserProfile.objects.get(user_id=request.user.id) 
    except:
         profile="Null"
    
    if request.method == 'POST':
        if 'rating' in request.POST:
          feed=feedback() 
          feed.snipId=id
          feed.ratting=request.POST['rating']
          feed.user_id=request.user.id           
          feed.save()    
        else:
          com=comment()
          com.user_id=request.user.id
          com.comment=request.POST['comment']
          com.user_name=request.user.username
          com.snipId=id
          com.save()
          

    else:
       print('error')

    snippet=snippets.objects.get(id=id)
    pId=request.user.id
    profile = UserProfile.objects.get(user_id=snippet.user_id)
    backfeed='NULL'
    try:
       backfeed = feedback.objects.get(user_id=request.user.id , snipId=id) 
    except:
       backfeed='NULL'

    try:
        viewCode = snippetsViewer.objects.get(user_id=pId , snipId=id)
    except:
        viewCode = []
        viewCode = 0
    comm='Null'
    try:
        comm=comment.objects.filter(snipId=id)

    except:
        print('error')        

    return render(request, 'snip_description.html' , {'snippet':snippet , 'profile':profile  
        ,  'viewCode' : viewCode ,  'backfeed':backfeed , 'comm' :comm })


def buySnip(request, id):
    if request.method == 'POST':

        if request.POST['Account'] == '1111-1111': 
             descri=snippetsViewer()
             descri.user_id=request.user.id    
             descri.buyer=1
             descri.snipId=id
             descri.save() 
        else:
            print('error')
            return redirect('home')

    return render(request, 'buySnippets.html', {})




def mySnips (request):
    snippet = snippets.objects.all()
    try:
       matched=[]
       for k in snippet:
           if  k.user_id == request.user.id :
               matched.append(k)
    except:
          print('error')
    
    return render(request, 'mysnippets.html', {'matched':matched})



def delete_snip(request , id):
    snippet = snippets.objects.get(id=id)

    if request.method == 'POST':
        snippet.delete()
        return redirect('mySnips')
    return render(request , 'mysnip-delete-confirm.html' , {})    


def  update_snip(request ,id):
     snippet=snippets.objects.get(id=id)

     if request.method=='POST':
         snippet.user_id=request.user.id
         snippet.description= request.POST['description']
         snippet.nafme= request.POST['name']
         snippet.code= request.POST['code']
         snippet.title = request.POST['title']
         snippet.image= snippet.image
         snippet.image_name=snippet.image.name
         snippet.save()
         return redirect('mySnips')

     return render(request, 'snippetUpdate-form.html', { 'snippet':snippet })


def requestform(request ,id):
    alert=0
    snippet=snippets.objects.get(id=id)
    if request.method=='POST':
         change=changerequest()
         change.snipId= id
         change.user_id=request.user.id
         change.description= request.POST['description']
         change.title = request.POST['title']
         change.owner_id = snippet.user_id 
         change.save()
         alert=1
    return render(request, 'requestform.html', { 'snippet':snippet  ,  'alert' : alert})     


def confirmation(request ,id):
    if request.method=='POST':
         
         chang=changerequest.objects.get(id=id)
         snipid=chang.snipId
         changerequest.objects.filter(owner_id=request.user.id ,snipId =snipid).update(status="Working")
         
         return redirect('/snippets/mySnips/updation/%i' %snipid)
    
    chang=changerequest.objects.get(id=id)
    return render(request, 'changerequest.html', { 'chang':chang  })  

def deletenotification(request ,id):
    chang=changerequest.objects.get(id=id)
    chang.delete()        
    return redirect('mySnips')  



def updation(request ,id):
    snippet=snippets.objects.get(id=id)

    if request.method=='POST':
        snippet.user_id=request.user.id
        snippet.description= request.POST['description']
        snippet.nafme= request.POST['name']
        snippet.code= request.POST['code']
        snippet.title = request.POST['title']
        snippet.image= snippet.image
        snippet.image_name=snippet.image.name
        snippet.times=snippet.times+1
        snippet.save()
        changerequest.objects.filter(owner_id=request.user.id ,snipId =id).update(status="Done")
        return redirect('mySnips')

    return render(request, 'updaterequest.html', { 'snippet':snippet })  

def snipsearch(request):
    template='all_snippets.html'
    query=request.GET.get('q')
    if query:
       snippet= snippets.objects.filter(Q(title__icontains=query) |  Q(description__icontains=query ) |  Q(Javascript__icontains=query)  |  Q(MEANStack__icontains=query ) |  Q(Django__icontains=query )  |  Q(asp__icontains=query ) |  Q(HTML__icontains=query ) |  Q(CSS__icontains=query ) |  Q(Php__icontains=query ) )
    else :
       print('there is nothing')

    if not snippet:
       snippet="None"

    return render(request , template , {'snippet':snippet})

@csrf_exempt
def downloads(request , id):
    nod = 0
    nod=snippets.objects.get(id=id).downloads
    nod += 1    
    snippets.objects.filter(id=id).update(downloads=nod) 
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
    

