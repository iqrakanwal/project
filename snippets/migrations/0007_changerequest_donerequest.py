# Generated by Django 2.0.7 on 2018-10-19 20:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('snippets', '0006_snippets_sniptype'),
    ]

    operations = [
        migrations.CreateModel(
            name='changerequest',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('snipId', models.IntegerField(default=0)),
                ('user_id', models.IntegerField(default=0)),
                ('status', models.CharField(default='', max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='donerequest',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('snipId', models.IntegerField(default=0)),
                ('user_id', models.IntegerField(default=0)),
                ('status', models.CharField(default='', max_length=100)),
            ],
        ),
    ]
