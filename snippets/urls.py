from django.urls import path

from . import views




urlpatterns = [
    path('snipresult/', views.snipsearch , name="snipsearch"),
    path('all/', views.snippet , name="snippets"),
    path('create/', views.createSnippet , name="create"),
    path('all/snip_descri/<int:id>/', views.snip_descri , name="snip_descri"),
    path('buySnip/<int:id>/', views.buySnip , name="buySnip"),
    path('mySnips/', views.mySnips , name="mySnips"),
    path('all/delete_snip/<int:id>/', views.delete_snip , name="delete_snip"),
    path('all/update_snip/<int:id>/', views.update_snip , name="update_snip"),
    path('all/requestform/<int:id>/', views.requestform , name="requestform"),
    path('mySnips/confirmation/<int:id>/', views.confirmation , name="confirmation"),
    path('mySnips/deletenotification/<int:id>/', views.deletenotification , name="deletenotification"),
    path('mySnips/updation/<int:id>/', views.updation , name="updation"),
    path('snipcategory/<slug:name>/', views.snipcategory , name="snipcategory"),
    path('all/downloads/<int:id>/', views.downloads , name="downloads"),

]  

