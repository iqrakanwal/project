from django.db import models

# Create your models here.


class snippets(models.Model):
	 Javascript=models.CharField(max_length=100 , default=0)
	 MEANStack=models.CharField(max_length=100 ,default=0)
	 Django=models.CharField(max_length=100 , default=0)
	 HTML=models.CharField(max_length=100 , default=0)
	 asp=models.CharField(max_length=100 , default=0)
	 CSS=models.CharField(max_length=100 ,default=0)
	 Php=models.CharField(max_length=100 , default=0)
     

	 codeoptimization=models.IntegerField(default=0)
	 user_id = models.IntegerField(default=0)
	 description = models.CharField(max_length=200 , default='')
	 name = models.CharField(max_length=100 , default='')
	 code = models.CharField(max_length=2000 , default='')	 
	 image=models.ImageField(upload_to='snippet_image' , blank=True)
	 usermanual = models.FileField(upload_to='um)', blank=True)
	 image_name=models.CharField(max_length=100 , default='')
	 title = models.CharField(max_length=100 , default='')
	 sniptype =  models.CharField(max_length=100 , default='')	
	 times = models.IntegerField(default=0)
	 downloads = models.IntegerField(default=0)
	 



class snippetsViewer(models.Model):
	snipId = models.IntegerField(default=0)
	user_id = models.IntegerField(default=0)
	buyer = models.IntegerField(default=0)


class changerequest(models.Model):
	title = models.CharField(max_length=100 , default='')
	owner_id = models.IntegerField(default=0)
	snipId = models.IntegerField(default=0)
	user_id = models.IntegerField(default=0)
	status = models.CharField(max_length=100 , default='request')
	description=models.CharField(max_length=100 , default='') 
    


class donerequest(models.Model):
	snipId = models.IntegerField(default=0)
	status = models.CharField(max_length=100 , default='')


class feedback(models.Model):
	snipId = models.IntegerField(default=0)
	ratting = models.IntegerField(default=0) 
	user_id = models.CharField(max_length=100 ,default=0)
    
class comment(models.Model):
     snipId = models.IntegerField(default=0)
     user_name = models.CharField(max_length=100 ,default=0) 
     user_id = models.IntegerField(primary_key=True)
     comment= models.CharField(max_length=100 ,default=0)